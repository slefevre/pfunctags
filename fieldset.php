<?php

function fieldset($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("fieldset", $content, $options);
}