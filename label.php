<?php

function label($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("label", $content, $options);
}