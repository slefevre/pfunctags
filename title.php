<?php

function title($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("title", $content, $options);
}