<?php

function pretty_print_html($html) {
  $doc = new DOMDocument();
  $doc->preserveWhiteSpace = FALSE;
  $doc->loadHTML($html);
  $doc->formatOutput = TRUE;
  return $doc->saveHTML();
}
