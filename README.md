# README #

### What is this repository for? ###

 * Creates functions for each html tag.
 * Version: alpha 
 * Requirements: php 5.3 

### Raison D'être ###

 * Allows templating in pure code more easily. 
 * Less typing over tag strings ( e.g. `table($content)` vs. `"<table>$content</table>"`
 * Validate HTML structure by validating php syntax (e.g. `php -l`)
 * Avoid mis-matched or unclosed tags and typos in tag names.

### How to use ###

 * Include the file `tag.php`. Then, either 1. include `anonymous.php` for anonymous functions, 2. include `functions.php` for all of the php functions, or 3. include only the tag function(s) you want, such as `html.php` or `td.php`.
 * The function for the tags `dir`, `dl`, `header`, `link`, `time`, and `var` are prefixed with an underscore (e.g. `_header()`) because those functions already exist in the PHP root namespace (and `var` is a language construct). However, the equivalent anonymous functions have no leading underscore (e.g. `$header()`).

### Examples ###

 * Including all functions: 
```php
require_once('tag.php');
require_once('functions.php');
echo html(head() . body());
```
Output:
```html
<html><head></head><body></body></html>
```

 * Using anonymous functions: 
```php
require_once('tag.php');
require_once('anonymous.php');
echo $html($head() . $body());
```
Output:
```html
<html><head></head><body></body></html>
```

 * Including only the functions you want: 
```php
require_once('tag.php');
require_once('html.php');
require_once('head.php');
require_once('body.php');
echo html(head() . body());
```
Output:
```html
<html><head></head><body></body></html>
```

 * Attributes :
```php

// In this example, `link` is prefixed with an underscore, 
// because the function link() is already defined in PHP

// Attributes are passed in the second argument as a string...
echo _link(NULL, 'type="text/css" rel="stylesheet" href="style.css"') . "\n";

// ... or an array, in the format `array('attribute'=>'value')`
echo _link(NULL, array('type'=>'text/css','rel'=>'stylesheet','href'=>'style.css')) . "\n";

```

```html
<link type="text/css" rel="stylesheet" href="style.css"/>
<link type='text/css' rel='stylesheet' href='style.css' />
``` 
### To-do ###

 * ~~make closing tags respect which tags don't need a separate closing tag. Does this vary per HTML version?~~
~~ http://stackoverflow.com/questions/3008593/html-include-or-exclude-optional-closing-tags ~~  Test this!
 * Remove `$content` argument from tags that self-close
 * Make `tag()` avoid extra spaces (is this worth it?)
 * Implement indentation ~~and line breaks~~
 * More testing

### Contribution guidelines ###

 * write tests in `tests/`.
 * Follow this convention:
```php
function func($arg1, $arg2) {
  something();
}
if ( $var ) {
  something();
} else {
  something($else);
}
```

### Who do I talk to? ###

 * lefevre.10@osu.edu
