<?php

function abbr($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("abbr", $content, $options);
}

function acronym($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("acronym", $content, $options);
}

function address($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("address", $content, $options);
}

function a($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("a", $content, $options);
}

function applet($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("applet", $content, $options);
}

function area($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("area", $content, $options);
}

function article($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("article", $content, $options);
}

function aside($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("aside", $content, $options);
}

function audio($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("audio", $content, $options);
}

function basefont($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("basefont", $content, $options);
}

function base($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("base", $content, $options);
}

function bdi($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("bdi", $content, $options);
}

function bdo($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("bdo", $content, $options);
}

function bgsound($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("bgsound", $content, $options);
}

function big($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("big", $content, $options);
}

function blink($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("blink", $content, $options);
}

function blockquote($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("blockquote", $content, $options);
}

function body($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("body", $content, $options);
}

function b($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("b", $content, $options);
}

function br($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("br", $content, $options);
}

function button($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("button", $content, $options);
}

function canvas($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("canvas", $content, $options);
}

function caption($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("caption", $content, $options);
}

function center($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("center", $content, $options);
}

function cite($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("cite", $content, $options);
}

function code($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("code", $content, $options);
}

function colgroup($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("colgroup", $content, $options);
}

function col($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("col", $content, $options);
}

function content($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("content", $content, $options);
}

function datalist($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("datalist", $content, $options);
}

function data($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("data", $content, $options);
}

function dd($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("dd", $content, $options);
}

function decorator($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("decorator", $content, $options);
}

function del($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("del", $content, $options);
}

function details($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("details", $content, $options);
}

function dfn($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("dfn", $content, $options);
}

function dialog($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("dialog", $content, $options);
}

function _dir($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("dir", $content, $options);
}

function div($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("div", $content, $options);
}

function _dl($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("dl", $content, $options);
}

function dt($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("dt", $content, $options);
}

function element($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("element", $content, $options);
}

function embed($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("embed", $content, $options);
}

function em($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("em", $content, $options);
}

function fieldset($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("fieldset", $content, $options);
}

function figcaption($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("figcaption", $content, $options);
}

function figure($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("figure", $content, $options);
}

function font($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("font", $content, $options);
}

function footer($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("footer", $content, $options);
}

function form($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("form", $content, $options);
}

function frame($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("frame", $content, $options);
}

function frameset($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("frameset", $content, $options);
}

function h1($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("h1", $content, $options);
}

function h2($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("h2", $content, $options);
}

function h3($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("h3", $content, $options);
}

function h4($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("h4", $content, $options);
}

function h5($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("h5", $content, $options);
}

function h6($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("h6", $content, $options);
}

function _header($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("header", $content, $options);
}

function head($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("head", $content, $options);
}

function hgroup($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("hgroup", $content, $options);
}

function hr($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("hr", $content, $options);
}

function html($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("html", $content, $options);
}

function iframe($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("iframe", $content, $options);
}

function img($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("img", $content, $options);
}

function input($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("input", $content, $options);
}

function ins($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("ins", $content, $options);
}

function i($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("i", $content, $options);
}

function isindex($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("isindex", $content, $options);
}

function kbd($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("kbd", $content, $options);
}

function keygen($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("keygen", $content, $options);
}

function label($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("label", $content, $options);
}

function legend($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("legend", $content, $options);
}

function _link($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("link", $content, $options);
}

function li($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("li", $content, $options);
}

function listing($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("listing", $content, $options);
}

function main($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("main", $content, $options);
}

function map($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("map", $content, $options);
}

function mark($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("mark", $content, $options);
}

function marquee($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("marquee", $content, $options);
}

function menuitem($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("menuitem", $content, $options);
}

function menu($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("menu", $content, $options);
}

function meta($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("meta", $content, $options);
}

function meter($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("meter", $content, $options);
}

function nav($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("nav", $content, $options);
}

function nobr($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("nobr", $content, $options);
}

function noframes($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("noframes", $content, $options);
}

function noscript($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("noscript", $content, $options);
}

function object($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("object", $content, $options);
}

function ol($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("ol", $content, $options);
}

function optgroup($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("optgroup", $content, $options);
}

function option($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("option", $content, $options);
}

function output($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("output", $content, $options);
}

function param($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("param", $content, $options);
}

function picture($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("picture", $content, $options);
}

function plaintext($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("plaintext", $content, $options);
}

function p($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("p", $content, $options);
}

function pre($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("pre", $content, $options);
}

function progress($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("progress", $content, $options);
}

function q($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("q", $content, $options);
}

function rp($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("rp", $content, $options);
}

function rt($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("rt", $content, $options);
}

function ruby($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("ruby", $content, $options);
}

function samp($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("samp", $content, $options);
}

function script($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("script", $content, $options);
}

function section($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("section", $content, $options);
}

function select($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("select", $content, $options);
}

function shadow($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("shadow", $content, $options);
}

function small($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("small", $content, $options);
}

function source($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("source", $content, $options);
}

function spacer($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("spacer", $content, $options);
}

function span($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("span", $content, $options);
}

function s($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("s", $content, $options);
}

function strike($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("strike", $content, $options);
}

function strong($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("strong", $content, $options);
}

function style($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("style", $content, $options);
}

function sub($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("sub", $content, $options);
}

function summary($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("summary", $content, $options);
}

function sup($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("sup", $content, $options);
}

function table($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("table", $content, $options);
}

function tbody($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("tbody", $content, $options);
}

function td($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("td", $content, $options);
}

function template($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("template", $content, $options);
}

function textarea($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("textarea", $content, $options);
}

function tfoot($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("tfoot", $content, $options);
}

function thead($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("thead", $content, $options);
}

function th($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("th", $content, $options);
}

function _time($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("time", $content, $options);
}

function title($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("title", $content, $options);
}

function track($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("track", $content, $options);
}

function tr($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("tr", $content, $options);
}

function tt($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("tt", $content, $options);
}

function ul($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("ul", $content, $options);
}

function u($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("u", $content, $options);
}

function _var($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("var", $content, $options);
}

function video($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("video", $content, $options);
}

function wbr($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("wbr", $content, $options);
}

function xmp($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("xmp", $content, $options);
}
