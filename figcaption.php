<?php

function figcaption($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("figcaption", $content, $options);
}