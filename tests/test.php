<?php

require_once('../tag.php');
require_once('../functions.php');
require_once('../pretty_print_html.php');

$output = 
audio(
  'Your browser does not support the '. code('audio') . ' element.' .
  source(NULL, 'src="foo.wav" type="audio/wav"')
, 'controls="controls"');

echo $output;

$output = div(div(NULL, array('class'=>'header')), array('class'=>'container'));

echo $output;

$output = img(NULL, array('src'=>'images/image.jpg','alt'=>'An image')) 
  . img(NULL, "src='images/another_image.png' alt='Another image'");

echo $output;

$output = _link(NULL, 'type="text/css" rel="stylesheet" href="style.css"');
echo $output;

$output = _link(NULL, array('type'=>'text/css','rel'=>'stylesheet','href'=>'style.css'));
echo $output;

$output = table(array(tr(array(td(array(1,2,3)), td(array(4,5,6)), td(array(7,8,9)) ))));
echo $output;

require_once('../anonymous.php');
$output = $table(array($tr(array($td(array(1,2,3)), $td(array(4,5,6)), $td(array(7,8,9)) ))));
echo $output;
