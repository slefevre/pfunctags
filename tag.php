<?php

namespace pfunctags;

function tag($element, $content = NULL, $options = NULL, $quote = "'") {

  // these are tags that don't take a closing tag, but a `/>`    
  static $forbidden;
  if ( ! isset($forbidden) ) {
    $forbidden = array(
      'img',
      'input',
      'br',
      'hr',
      'frame',
      'area',
      'base',
      'basefont',
      'col',
      'isindex',
      'link',
      'meta',
      'param',
    );
  }

  // figure out options if they were passed.
  $options_output = "";
  if ( is_string($options) ) {
    $options_output = $options;
  } elseif ( is_array($options) ) {
    foreach ( $options as $key => $value ) {
      if ( strlen($key) == 0 ) {
        $options_output .= " $value ";
      } elseif ( strlen($value) == 0 ) {
        $options_output .= " $key ";
      } else {
        $options_output .= " $key=$quote$value$quote ";
      }
    }
  }
 
  // if content is an array, tag each element.
  if ( is_array($content) ) {
    $output = "";
    foreach ( $content as $piece ) {
      $output .= \pfunctags\tag($element, $piece, $options, $quote);
    }
  } elseif ( in_array($element, $forbidden) ) {
    $output = "<$element $options_output/>\n";
  } else {
    // double-linebreaks are getting in here somehow, strip them out
    $output = str_replace("\n\n", "\n", "<$element $options_output>\n$content\n</$element>\n");
  }

  // remove double spaces
#  $output = preg_replace('/  +/', ' ', $output); 

  return $output;
}
