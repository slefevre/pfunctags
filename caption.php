<?php

function caption($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("caption", $content, $options);
}