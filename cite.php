<?php

function cite($content = NULL, $options = NULL, $quote = "'") {
  return \pfunctags\tag("cite", $content, $options);
}